package arrays;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args){
		
		//DECLARACION Y/O INICIALIZACION DE VARIABLES
		Scanner sc = new Scanner(System.in);
		boolean salir;
		int tamaño=0,n=0;
		
		do{
			try{
				System.out.print("Tamaño array: ");
				tamaño = sc.nextInt();
				salir=false;
			}catch(Exception e){
				salir=true;
			}finally{
				sc.nextLine();
			}		
		}while(salir);	
		
		int num[] = new int[tamaño>=0?tamaño:tamaño*-1];
		
		do{
			try{
				System.out.print("Multiplos: ");
				n = sc.nextInt();
				salir=false;
			}catch(Exception e){
				salir=true;
			}finally{
				sc.nextLine();
			}		
		}while(salir);
		
		
		//OPERACIONES
		for(int i=1;i<num.length;i++)
			num[i-1]=i*n;
		
		//RESULTADO
		for(int i=0;i<num.length;i++)
			System.out.println(num[i]);
				
	}	
}