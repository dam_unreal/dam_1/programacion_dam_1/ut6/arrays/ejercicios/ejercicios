package arrays;

public class Ejercicio04 {
	public static void main(String[] args) {

		//DECLARACION Y ESTANCIACION
		int num[] = new int[100];
		double media=0;
		int contador=0;
				
		for(int i=0;i<num.length;i++)
			num[i]=(int)(Math.random()*99)+1;
		
				
		//OPERACION
		for(int i=0;i<num.length;i++){
			media+=num[i];
		}
		
		media = media/num.length;
		
		for(int i=0;i<num.length;i++){
			if(num[i]>media)
				contador++;
		}

		//RESULTADO	
		System.out.println("Hay un "+(double) contador/num.length*100+"% de personas que superan la media");
		
	}
}