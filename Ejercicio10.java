package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio10 {

	public static void main(String[] args) {
		
		//INICIALIZAR
		Scanner sc = new Scanner(System.in);
		int num[] = new int[100];
		int posicion=0, valor=0;
		boolean salir;
		
		//ASEGURAR VALORES VALIDOS
		do{
			try{
				System.out.print("Posicion: ");
				posicion = sc.nextInt();
				if(posicion<0)
					salir=true;
				else
					salir=false;
			}catch(Exception e){
				salir=true;
			}finally{
				sc.nextLine();
			}		
		}while(salir);
		
		do{
			try{
				System.out.print("Valor: ");
				valor = sc.nextInt();
				salir=false;
			}catch(Exception e){
				salir=true;
			}finally{
				sc.nextLine();
			}		
		}while(salir);
		
		//DAR VALORES
		for(int i=0;i<num.length;i++)
			num[i] = i;
		
		//MODIFICAR
		System.out.println(Arrays.toString(num));
		for(int i=num.length-1;i>posicion;i--)
			num[i] = num[i-1];
		
		num[posicion] = valor;
		
		System.out.println(Arrays.toString(num));	
	}
}