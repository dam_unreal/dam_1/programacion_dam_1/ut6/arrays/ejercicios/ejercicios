package arrays;

public class Ejercicio09 {

	public static void main(String[] args) {
		
		//INICIALIZAR
		int num[] = new int[100];
		int sumaPares=0,sumaImpares=0;
 
		
		//Dar valores
		for(int i=0;i<num.length;i++)
			num[i] = (int)(Math.random()*9)+1;
		
		//OPERAR
		for(int i=0;i<num.length;i++)
			if(i%2==0){
				sumaPares+=num[i];
			}else{
				sumaImpares+=num[i];
			}
	
		System.out.println("Suma pares: "+sumaPares);
		System.out.println("Suma impares: "+sumaImpares);
		
	}
	
}