package arrays;

import java.util.Scanner;

public class Ejercicio06 {

	public static void main(String[] args){
		
		//INICIALIZAR
		Scanner sc = new Scanner(System.in);
		int v1[],v2[],v3[], tamaño=0;
		boolean salir;
		
		do{
			try{
				System.out.println("Tamaño array: ");
				tamaño = sc.nextInt();
				salir=false;
			}catch(Exception e){
				salir=true;
			}finally{
				sc.nextLine();
			}		
		}while(salir);
		
		v1 = new int [tamaño];
		v2 = new int [tamaño];
		v3 = new int [tamaño];
		
		//DAR VALORES
		
		for(int i=0;i<v1.length;i++){
			v1[i] = (int)(Math.random()*9)+1;
			v2[i] = (int)(Math.random()*9)+1;
			v3[i] = v1[i] + v2[i];
		}
			
		System.out.print("Array 1: "+v1[0]);
		for(int i=1;i<v1.length;i++)
			System.out.print("-"+v1[i]);
		System.out.print("\nArray 1: "+v2[0]);
		for(int i=1;i<v1.length;i++)
			System.out.print("-"+v2[i]);
		System.out.print("\nArray 1: "+v3[0]);
		for(int i=1;i<v1.length;i++)
			System.out.print("-"+v3[i]);

		sc.close();
	}
	
}
