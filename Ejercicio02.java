package arrays;

import java.util.Scanner;

public class Ejercicio02 {
	public static void main(String[] args) {
		
		//DECLARACION Y/O INICIACION DE VARIABLES
		Scanner sc = new Scanner(System.in);
		int num[] = new int[5];
		
		//OPERACION
		for(int i=0;i<num.length;i++){
			System.out.print("Numero: ");
			num[i] = sc.nextInt();
		}
			
		//RESULTADO
		for(int i=0;i<num.length;i++)
			System.out.println(num[i]);
		
	}
}
