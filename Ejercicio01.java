package arrays;

public class Ejericio01 {
	public static void main(String[] args) {
		
		//INSTANCIAR Y DECLARACION
		int numero[] = new int[5];
		int numero2[] = {0,10,20,30,40};
 		
		//DECLARACION
		int numeros[];
		
		//INSTANCIACION	
		numeros = new int[5];

		//DAR VALORES
		
		for(int i=0;i<5;i++)
			numeros[i] = 5;
		
		//MOSTRAR VALORES
		
		for(int i=0;i<5;i++)
			System.out.println(numeros[i]);

	}
}