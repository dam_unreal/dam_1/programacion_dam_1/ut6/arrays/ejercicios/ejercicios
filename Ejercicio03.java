package arrays;

public class Ejercicio03 {

	public static void main(String[] args) {
		
		//DECLARACION Y ESTANCIACION
		int num[] = new int[5];
		double media=0;
		
		for(int i=0;i<num.length;i++)
			num[i]=(int)(Math.random()*99+1);
		
		//OPERACION
		for(int i=0;i<num.length;i++){
			media+=num[i];
		}

		//RESULTADO
		System.out.println("Media: "+media/num.length);	
	}
}