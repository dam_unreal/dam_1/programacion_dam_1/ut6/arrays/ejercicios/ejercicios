package arrays;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		//INICIALIZAR
		int notas[] = new int [10];
		double promedio=0;
		int aprobados=0 ,suspensos=0;
			
		//DAR VALORES
		for(int i=0;i<notas.length;i++)
			notas[i] = (int)(Math.random()*9)+1;
		
		//OPERAR
		for(int i=0;i<notas.length;i++){		
			promedio+=notas[i];
			
			if(notas[i]>=5)
				aprobados++;
			else
				suspensos++;		
		}
		promedio/=notas.length;
		
		System.out.println("Nota promedio: "+promedio);
		System.out.println("Aprobados: "+aprobados);
		System.out.println("Suspensos: "+suspensos);
	}

}